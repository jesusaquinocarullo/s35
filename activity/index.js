const express = require("express");

const mongoose = require("mongoose");

const app = express();

const port = 3003;

mongoose.connect(`mongodb+srv://jcarullo:Doomsdays123@cluster0.srmn8hn.mongodb.net/S36?retryWrites=true&w=majority`, {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

let db = mongoose.connection

db.on('error', console.error.bind(console, "Connection"));
db.once('open', () => console.log('Connected to MongoDB!'))

const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

const User = mongoose.model('User', userSchema);

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.post('/sign-up', (req, res) => {
	// business logic
	User.findOne({username: req.body.username}, (error, result) => {
		if(error) {
			return res.send(error)
		} else if (result != null && result.username == req.body.username) {
			return res.send('Duplicate account found')
		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})

			newUser.save((error, savedUser) => {
				if(error) {
					return console.error(error)
				} else {
					return res.status(201).send('New Account Created')
				}
			})
		}
	})
})

app.get('/users', (req, res) => {
	User.find({}, (error, result) => {
		if(error){
			return res.send(error)
		} else {
			return res.status(200).json({
				users: result
			})
		}
	})
})



app.listen(port, () => console.log(`Server is running at port: ${port}`));

