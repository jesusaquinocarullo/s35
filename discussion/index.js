// // Express Setup

// const express = require("express");

// // Mongoose is a package that allows creation of Schemas to model our data structures
// const mongoose = require("mongoose");

// const app = express();

// const port = 3001;

// // [Section] - Mongoose Connection
// // Mongoose uses the 'connect' function or method to connect to the cluster in MongoDB Atlas

// /*
// 	It takes 2 arguments:
// 	1. Connection String from our MongoDB Atlas.
// 	2. Object that contains the middlewares/standards that MongoDB uses.
// */

// mongoose.connect(`mongodb+srv://jcarullo:Doomsdays123@cluster0.srmn8hn.mongodb.net/S35?retryWrites=true&w=majority`, {
// 		// {newUrlParser: true} - it allows us to avoid any current and/or future error while connecting to MongoDB
// 		useNewUrlParser: true,

// 		// {useUnifiedTopology} is "false" by default. Set to true to opt in to using the MongoDB driver's new connection management engine
// 		useUnifiedTopology: true
// });

// // Initializes the mongoose connection to the MongoDB database by assigning "mongoose.connection" to the db variable
// let db = mongoose.connection

// // Listen to the events of the connection by using the 'on()' function of the mongoose connection and logs the details in the console based on the event (error or successful)

// // will always show once there's error
// db.on('error', console.error.bind(console, "Connection"));

// // will show only once when connected
// db.once('open', () => console.log('Connected to MongoDB!'))


// // CREATING A SCHEMA
// const taskSchema = new mongoose.Schema({
// 	// Define the fields with their corresponding data types
// 	// For a task, it needs a "task name" and its data type will be "String"
// 	name: String,
// 	// There is a field called "status" that has a data type of "String" and the default value is "pending"
// 	status: {
// 		type: String,
// 		default: 'Pending'
// 	}
// })

// // MODELS
// // The variable/object "Task" can now be used to run commands for interacting with our database.
// // "Task" is capitalized following the MVC approach for naming convention
// // Models must be in singular form and capitalized
// // The first parameter is used to specify the name of the collection where we will store our data
// // The second parameter is used to specify the Schema/blueprint of the documents that will be stored in our MongoDB collection/s
// const Task = mongoose.model('Task', taskSchema);

// // CREATING ROUTES

// // Middleware
// app.use(express.json());
// app.use(express.urlencoded({extended: true}));

// app.post('/tasks', (req, res) => {
// 	// business logic
// 	Task.findOne({name: req.body.name}, (error, result) => {
// 		if(error) {
// 			return res.send(error)
// 		} else if (result != null && result.name == req.body.name) {
// 			return res.send('Duplicate task found')
// 		} else {
// 			let newTask = new Task({
// 				name: req.body.name
// 			})

// 			newTask.save((error, savedTask) => {
// 				if(error) {
// 					return console.error(error)
// 				} else {
// 					return res.status(201).send('New Task Created')
// 				}
// 			})
// 		}
// 	})
// })


// app.get('/tasks', (req, res) => {
// 	Task.find({}, (error, result) => {
// 		if(error){
// 			return res.send(error)
// 		} else {
// 			return res.status(200).json({
// 				tasks: result
// 			})
// 		}
// 	})
// })












// app.listen(port, () => console.log(`Server is running at port: ${port}`));



// -------------------------------------------------------------------------
// 					       - WITHOUT NOTES - 

const express = require("express");

const mongoose = require("mongoose");

const app = express();

const port = 4001;

mongoose.connect(`mongodb+srv://jcarullo:Doomsdays123@cluster0.srmn8hn.mongodb.net/S35?retryWrites=true&w=majority`, {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

let db = mongoose.connection

db.on('error', console.error.bind(console, "Connection"));
db.once('open', () => console.log('Connected to MongoDB!'))

const userSchema = new mongoose.Schema({
	name: String,
	password: String,
	status: {
		type: String,
		default: 'Pending'
	}
})

const User = mongoose.model('User', userSchema);

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.post('/sign-up', (req, res) => {
	// business logic
	User.findOne({name: req.body.name}, (error, result) => {
		if(error) {
			return res.send(error)
		} else if (result != null && result.name == req.body.name) {
			return res.send('Duplicate account found')
		} else {
			let newUser = new User({
				name: req.body.name,
				password: req.body.password
			})

			newUser.save((error, savedUser) => {
				if(error) {
					return console.error(error)
				} else {
					return res.status(201).send('New Account Created')
				}
			})
		}
	})
})

app.get('/Users', (req, res) => {
	User.find({}, (error, result) => {
		if(error){
			return res.send(error)
		} else {
			return res.status(200).json({
				users: result
			})
		}
	})
})




app.listen(port, () => console.log(`Server is running at port: ${port}`));



